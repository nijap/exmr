from django.contrib import admin

from apps.common.models import Currency, SocialLink

admin.site.register(Currency)
admin.site.register(SocialLink)
